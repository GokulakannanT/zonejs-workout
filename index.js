
function getZone() {
    if (!global["Zone"]) {
      require("zone.js");
    }
    return global["Zone"];
}

const Zone = getZone();

class ZoneSpec {
    constructor(name) {
      this.name = name;
      this.properties = {};
      this._zoneTasks = [];
    }

    onHandleError(parentZoneDelegate, currentZone, targetZone, error) {
      console.log(error);
    }

    onInvokeTask(parentZoneDelegate, currentZone, targetZone, task, ...args) {
      return parentZoneDelegate.invokeTask(targetZone, task, ...args);
    }

    onScheduleTask(parentZoneDelegate, currentZone, targetZone, task) {
      this._zoneTasks.push(task);
      return parentZoneDelegate.scheduleTask(targetZone, task);
    }

    cleanup() {
      while (this._zoneTasks.length) {
        let task;
        try {
          task = this._zoneTasks.pop();
          if (task.state != `notScheduled`) Zone.current.cancelTask(task);
        } catch (e) {
          console.error("error::" + e.message);
        }
      }
    }
  }

  const spec = new ZoneSpec("async-task");

  const asyncZone = Zone.current.fork(spec);

  const macroTask = () => {
    setInterval(() => {
      console.log("async task");
    }, 1000);
  };

  const microTask = () => {
    Promise.resolve(0).then(() => {
      macroTask();
    });
  };

  asyncZone.run(microTask);

  setTimeout(() => {
    Zone.current.runGuarded(() => {
      spec.cleanup();
    });
  }, 5000);
